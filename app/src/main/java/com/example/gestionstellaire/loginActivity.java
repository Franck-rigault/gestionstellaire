package com.example.gestionstellaire;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.Key;
import java.util.HashMap;
import java.util.Map;

public class loginActivity extends AppCompatActivity {

    RequestQueue user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.connexion);
        user= Volley.newRequestQueue(this);
    }

    public void login(View v)
    {
        EditText etUsername = findViewById(R.id.connexion_nom);
        EditText etPassword = findViewById(R.id.connexion_mdp);

        String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();

        if(password.isEmpty() || username.isEmpty()){
            Toast.makeText(this,"Mauvaise saisie",Toast.LENGTH_LONG).show();
            return;
        }
        String url="http://10.0.2.2/~franck.rigault/Stellaire/le-stellaire/public/weblogin";
        StringRequest req =new StringRequest(Request.Method.POST,url,this::processLoginRequest,this::handleErrors){
            @Nullable
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap hm=new HashMap();
                hm.put("nomUtilisateur",username);
                hm.put("motDePasse",password);
                return hm;
            }
        };
        user.add(req);
    }

    private void processLoginRequest(String response) {
        try {
            JSONObject jo = new JSONObject(response);
            String token = jo.getString("access_token");
            Intent i = new Intent(this,menuActivity.class);
            i.putExtra("token", token);
            startActivity(i);
        } catch (JSONException x) {
            Toast.makeText(this, "JSON PARSE ERROR", Toast.LENGTH_LONG);
            Log.e("HELLOJWT", "exception attrapée dans processLoginRequest" );

            Log.e("HELLOJWT", x.getClass().getSimpleName());
            x.printStackTrace();
        }
    }

    public void handleErrors(Throwable t){
        Toast.makeText(this,"SERVERSIDE PROBLEM",Toast.LENGTH_LONG);
        Log.e("HELLOJWT","SERVERSIDE BUG",t);
    }
}