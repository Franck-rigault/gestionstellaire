package com.example.gestionstellaire;

public class Commande {

    String commande;

    public Commande(String commande) {this.commande = commande;}

    public String getCommande() {return commande;}

    public void setCommande(String commande) {this.commande = commande;}
}
