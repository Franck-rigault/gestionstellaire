package com.example.gestionstellaire;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

public class CommandeViewHolder extends RecyclerView.ViewHolder {
    View activity_listing_commande;
    TextView num_commande;

    public CommandeViewHolder(View v, final ICommandeViewListener listener){
        super(v);
        num_commande= v.findViewById(R.id.num_commande);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.clicItem(getAdapterPosition(),v);
            }
        });
    }

    public void bindData(final Commande gl) {
        num_commande.setText(gl.getCommande());
    }

}