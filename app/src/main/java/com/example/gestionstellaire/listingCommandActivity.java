package com.example.gestionstellaire;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class listingCommandActivity  extends AppCompatActivity implements ICommandeViewListener {
    RequestQueue user;
    String token;

    List<Commande> cli;
    RequestQueue fileRequeteWS;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listing_commande);

        user = Volley.newRequestQueue(this);
        Intent i = getIntent();
        token = i.getStringExtra("token");
        Log.i("HELLOJWT", "token " + token);

        RecyclerView rvCommande = (RecyclerView) findViewById(R.id.rvcommande);

        cli= new ArrayList<Commande>();
        CommandeAdapter adapter = new CommandeAdapter(cli);
        adapter.setListener(this);
        rvCommande.setAdapter(adapter);
        rvCommande.setLayoutManager(new LinearLayoutManager(this));
        fileRequeteWS= Volley.newRequestQueue(this);
        requestCommande();
    }

    public void requestCommande(){
        String url="http://10.0.2.2/~franck.rigault/Stellaire/le-stellaire/public/wsaffichecommande";
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                url,
                this::processCommande,
                this::gereErreurWS);

        fileRequeteWS.add(stringRequest);
    }

    @Override
    public void clicItem(int pos, View v) {
        Commande c=cli.get(pos);
        Toast.makeText((getApplicationContext()), "Commande", Toast.LENGTH_LONG);
    }

    public void processCommande(String reponse){
        try {
            JSONArray ja = new JSONArray(reponse);
            String rep = "";

            for (int i = 0; i < ja.length(); i++) {

                //CHANGE /!\
                String res = ja.getJSONObject(i).getString("idCommande");


                cli.add(new Commande(res));
            }

            CommandeAdapter adapter = new CommandeAdapter(cli);
            //je recupere la recyclerview
            RecyclerView rvCommande = (RecyclerView) findViewById(R.id.rvcommande);
            //je fais setadapter et setlayoutmanager
            rvCommande.setAdapter(adapter);
            rvCommande.setLayoutManager(new LinearLayoutManager(this));

        } catch (JSONException jx) {

            gereErreurWS(jx);

        }
    }
    public void gereErreurWS(Throwable t){

        Toast.makeText(this, "Problème de communication avec le serveur", Toast.LENGTH_LONG).show();

        Log.e("Commande", "Problème de communication avec le serveur", t);

    }




}
