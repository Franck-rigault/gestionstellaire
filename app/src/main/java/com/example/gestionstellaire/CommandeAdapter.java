package com.example.gestionstellaire;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class CommandeAdapter extends RecyclerView.Adapter<CommandeViewHolder> {
    private List<Commande> mescommandes;
    public CommandeAdapter(List<Commande> lp) {mescommandes = lp; }
    ICommandeViewListener listener;

    public void setListener(ICommandeViewListener l){
        listener=l;
    }

    @NonNull
    @Override
    public CommandeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View sous_vue = inflater.inflate(R.layout.resume_commande, parent ,false);


        CommandeViewHolder viewHolder = new CommandeViewHolder(sous_vue,listener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CommandeViewHolder holder, int position){
        Commande commande=mescommandes.get(position);
        holder.bindData(commande);
    }

    @Override
    public int getItemCount() {
        return mescommandes.size();
    }

}
