package com.example.gestionstellaire;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class deleteProduitActivity extends AppCompatActivity {
    RequestQueue user;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.supprimer_produit);

        user = Volley.newRequestQueue(this);
        Intent i = getIntent();
        token = i.getStringExtra("token");
        Log.i("HELLOJWT", "token " + token);
    }

    public void delete_produit(View v) {
        EditText etIdProduit = v.findViewById(R.id.supprimer_id);
        EditText etVerif = v.findViewById(R.id.supprimer_cle);

        String IdProduit = etIdProduit.toString();
        int Id = Integer.parseInt(IdProduit);
        String Cle = etVerif.toString();

        if(Cle.isEmpty() || Cle!="Verif"){
            Toast.makeText(this,"Mauvaise saisie ou mauvaise clé",Toast.LENGTH_LONG).show();
            return;
        }

        String url="http://10.0.2.2/~franck.rigault/Stellaire/le-stellaire/public/wsdeleteproduit";
        StringRequest req =new StringRequest(Request.Method.POST,url,this::processAddProductRequest,this::handleErrors){
            @Nullable
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap hm=new HashMap();
                hm.put("idProduit",Id);
                return hm;
            }
        };
        user.add(req);
    }
    private void processAddProductRequest(String response) {
        try {
            JSONObject jo = new JSONObject(response);
            String token = jo.getString("access_token");
            Intent i = new Intent(this,addProduitActivity.class);
            i.putExtra("token", token);
            startActivity(i);
        } catch (JSONException x) {
            Toast.makeText(this, "JSON PARSE ERROR", Toast.LENGTH_LONG);
            Log.e("HELLOJWT", "exception attrapée dans processAddProductRequest" );

            Log.e("HELLOJWT", x.getClass().getSimpleName());
            x.printStackTrace();
        }
    }
    public void handleErrors(Throwable t){
        Toast.makeText(this,"SERVERSIDE PROBLEM",Toast.LENGTH_LONG);
        Log.e("HELLOJWT","SERVERSIDE BUG",t);
    }
}
