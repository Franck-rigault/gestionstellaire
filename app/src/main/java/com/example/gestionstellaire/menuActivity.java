package com.example.gestionstellaire;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class menuActivity extends AppCompatActivity {
    RequestQueue user;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);
        user = Volley.newRequestQueue(this);
        Intent i = getIntent();
        token = i.getStringExtra("token");
        Log.i("HELLOJWT", "token " + token);
    }


    public void accesCommandes(View v) {
        Intent c = new Intent(menuActivity.this, listingCommandActivity.class);
        startActivity(c);
    }
    public void accesgestion(View v){
        Intent i= new Intent(menuActivity.this, optionGestionActivity.class);
        startActivity(i);
    }

}