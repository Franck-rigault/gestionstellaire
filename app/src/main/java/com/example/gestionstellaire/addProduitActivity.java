package com.example.gestionstellaire;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class addProduitActivity extends AppCompatActivity {
    RequestQueue user;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ajouter_produit);

        user = Volley.newRequestQueue(this);
        Intent i = getIntent();
        token = i.getStringExtra("token");
        Log.i("HELLOJWT", "token " + token);

    }


    public void add_produit(View v) {
        EditText etNomproduit = v.findViewById(R.id.add_nom);
        EditText etDesc = v.findViewById(R.id.add_description);
        EditText etPhoto = v.findViewById(R.id.add_photo);
        EditText etPrix = v.findViewById(R.id.add_prix);

        String Nomproduit = etNomproduit.toString();
        String Description = etDesc.toString();
        String photo = etPhoto.toString();
        String prix = etPrix.toString();

        if(Nomproduit.isEmpty() || Description.isEmpty() || prix.isEmpty() || photo.isEmpty()){
            Toast.makeText(this,"Mauvaise saisie",Toast.LENGTH_LONG).show();
            return;
        }

        String url="http://10.0.2.2/~franck.rigault/Stellaire/le-stellaire/public/wsajouterproduit";
        StringRequest req =new StringRequest(Request.Method.POST,url,this::processAddProductRequest,this::handleErrors){
            @Nullable
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap hm=new HashMap();
                hm.put("nomProduit",Nomproduit);
                hm.put("descriptionProduit",Description);
                hm.put("prixProduit",prix);
                hm.put("imgSrc",photo);
                return hm;
            }
        };
        user.add(req);
    }
    private void processAddProductRequest(String response) {
        try {
            JSONObject jo = new JSONObject(response);
            String token = jo.getString("access_token");
            Intent i = new Intent(this,addProduitActivity.class);
            i.putExtra("token", token);
            startActivity(i);
        } catch (JSONException x) {
            Toast.makeText(this, "JSON PARSE ERROR", Toast.LENGTH_LONG);
            Log.e("HELLOJWT", "exception attrapée dans processAddProductRequest" );

            Log.e("HELLOJWT", x.getClass().getSimpleName());
            x.printStackTrace();
        }
    }
    public void handleErrors(Throwable t){
        Toast.makeText(this,"SERVERSIDE PROBLEM",Toast.LENGTH_LONG);
        Log.e("HELLOJWT","SERVERSIDE BUG",t);
    }
}

