package com.example.gestionstellaire;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class optionGestionActivity extends AppCompatActivity {
    RequestQueue user;
    String token;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.options_gestion);

        user = Volley.newRequestQueue(this);
        Intent i = getIntent();
        token = i.getStringExtra("token");
        Log.i("HELLOJWT", "token " + token);
    }

    public void accesAddProduit(View v) {
        Intent i = new Intent(optionGestionActivity.this, addProduitActivity.class);
        startActivity(i);
    }
    public void accesDeleteProduit(View v){
        Intent i= new Intent(optionGestionActivity.this, deleteProduitActivity.class);
        startActivity(i);
    }

    public void accesStock(View v){
        Intent i= new Intent(optionGestionActivity.this, addStockActivity.class);
        startActivity(i);
    }
}